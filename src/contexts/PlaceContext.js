import React,{createContext} from 'react';
import { REACT_APP_API_URL } from '@env';
import lodash from 'lodash'

const initialAuth = {
    getAllPlaces: () => {},
    addPlaces: () => {}
}

export const PlaceContext = createContext(initialAuth)

export const PlaceProvider = ({children}) => {

     //Récupère tout les lieux
     const getAllPlaces = (id) =>{
        const places = fetch(REACT_APP_API_URL+"/myPlaces/"+id,{
            method:'GET',
            headers:{
                "Content-Type":"application/json",
            },
        })
        .then((response)=>{
            return response.json()
        })
        .then((json)=>{
         
            lodash.forEach(json,(m)=>{
                let total = 0
                lodash.forEach(m.avis,(a)=>{
                    total+= a.rate
                })
                if(total == 0 ){
                    m.rate = total
                }else{
                    m.rate = Math.round(total/m.avis.length)
                }
            })
            return json
        }).catch((error)=>{
            console.log(error)
        })
        return places
    }

    //Ajoute un lieux
    const addPlaces = (newPlaces) =>{
        const add = fetch(REACT_APP_API_URL+"/place",{
            method:'POST',
            headers:{
                "Content-Type":"application/json",
            },
            body:JSON.stringify(newPlaces)
        })
        .then((response)=>{
            return response.json()
        })
        .then((json)=>{
            console.log(json)
            return json

        }).catch((error)=>{
            console.log(error)
        })
        return add
    }
    
    return (
        <PlaceContext.Provider value={{
            getAllPlaces,
            addPlaces,
        }}>
            {children}
        </PlaceContext.Provider>
    )
}