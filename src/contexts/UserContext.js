import React,{createContext, useState} from 'react';
import Loading from '../screens/Loading';
import { REACT_APP_API_URL } from '@env';

const initialUser = {
    isLoading:true,
    getUser: () => {},
    updateUser: () => {},
}

export const UserContext = createContext(initialUser)

export const UserProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(false);
    const getUser = (userId) => {
        // console.log(`${REACT_APP_API_URL}/people/${userId}`)

        const user = fetch(`${REACT_APP_API_URL}/people/${userId}`,{
            method:'GET',
            headers:{
                'Content-Type': 'application/json',
            },
        })
        .then((response)=> {
            return response.json();
        })
        .catch((error)=>{
            console.error(error);
        });
        return user;
    };

    const updateUser = (userId, user) => {
        let formdata = new FormData();
        formdata.append("firstname", user.firstname)
        formdata.append("lastname", user.lastname)
        formdata.append("login", user.login)
        formdata.append("password", user.password)
        if(!user.avatar.includes('/avatars')) {
            formdata.append("avatar", {uri: user.avatar, name: `${user.login}.jpg`, type: 'image/jpeg'})
        } else {
            formdata.append("avatar", user.avatar);
        }

        const userUpdate = fetch(`${REACT_APP_API_URL}/people/${userId}`, {
            method: 'PUT',
            headers:{
                'Content-Type': 'multipart/form-data',
            },
            body: formdata,
        }).then((response) => {
            return response.json();
        }).catch((error)=>{
            console.error(error);
        });
        return userUpdate;
    };

    return (
        <UserContext.Provider value={{
            isLoading,
            getUser,
            updateUser,
        }}>
            {isLoading ? <Loading/> : children }
        </UserContext.Provider>
    )
}