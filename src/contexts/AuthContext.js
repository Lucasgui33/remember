import React,{createContext, useState,useEffect} from 'react';
import Loading from '../screens/Loading';
import { REACT_APP_API_URL } from '@env';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ToastAndroid } from 'react-native';

const initialAuth = {
    isConnected:false,
    isLoading:true,
    login: () => {},
    logout: () => {},
    register: () => {},
}

export const AuthContext = createContext(initialAuth)

export const AuthProvider = ({children}) => {

    const [isConnected, setIsConnected] = useState(false)
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        checkIsConnected()
    }, []);

    const checkIsConnected = async () => {
        if(await AsyncStorage.getItem('userId')) {
            setIsConnected(true);
            setIsLoading(false)
        }else{
            setIsConnected(false)
            setIsLoading(false)
        }
    }

    const login = (connexion) => {
        const user = fetch(`${REACT_APP_API_URL}/login`,{
            method:'POST',
            headers:{
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(connexion),
        })
        .then((response)=> {
            return response.json();
        })
        .then(async (json)=>{
            await AsyncStorage.setItem('userId', json['id'].toString());
            await AsyncStorage.setItem('avatarPath', json['avatarPath']);
            if(await AsyncStorage.getItem('userId') === json['id'].toString()) {
                setIsConnected(true);
            }
            return json;
        }).catch((error)=>{
            console.log(error);
            ToastAndroid.show("Login failed : Email or password is incorrect", ToastAndroid.LONG);
        });
        return user;
    }
    const logout = async ({navigation}) => {
        await AsyncStorage.removeItem('userId');
        await AsyncStorage.removeItem('avatarPath');
        setIsConnected(false);
    }

    const register = (user) => {
        let formdata = new FormData();
        formdata.append("firstname", user.firstname)
        formdata.append("lastname", user.lastname)
        formdata.append("pseudo", user.login)
        formdata.append("password", user.password)
        formdata.append("avatar", {uri: user.avatar, name: `${user.login}.jpg`, type: 'image/jpeg'})

        console.log(formdata);
        console.log(REACT_APP_API_URL);
        const newUser = fetch(`${REACT_APP_API_URL}/people`,{
            method:'POST',
            headers:{
                'Content-Type': 'multipart/form-data',
            },
            body: formdata,
        })
        .then((response)=> {
            return response.json();
        })
        .then((json)=>{
            console.log("JSON : ",json);
            return json;
        }).catch((error)=>{
            console.log(error);
        });
        return newUser;
    };
    
    return (
        <AuthContext.Provider value={{
            isConnected,
            isLoading,
            login,
            logout,
            register,
        }}>
            {isLoading ? <Loading/> : children }
        </AuthContext.Provider>
    )
}