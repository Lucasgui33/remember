import React, { useContext, useState } from "react";

import {
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { tailwind } from "../../../tailwind";
import { AuthContext } from "../../contexts/AuthContext";

const Login = ({ navigation }) => {
    const { login } = useContext(AuthContext);
    const [connexion, setConnexion] = useState({
      login: "Liittlefoxx",
      password: "1234",
    });

    const submit = async () => {
      setIsSubmitted(true);
      await login(connexion);
    }
    const [isSubmitted, setIsSubmitted] = useState(false);

    return (
        <ScrollView>
          <Text style={tailwind("text-base text-center m-2")}>Pseudo (Login)</Text>
          <TextInput
            onChangeText={(e) => setConnexion({ ...connexion, login: e })}
            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
            value={connexion.login}
          />
          {(isSubmitted && !connexion.login.length) && (
            <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
          )}
          
          <Text style={tailwind("text-base text-center m-2")}>Mot de passe</Text>
          <TextInput
            onChangeText={(e) => setConnexion({ ...connexion, password: e })}
            secureTextEntry
            value={connexion.password}
            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
          />
           {(isSubmitted && !connexion.password.length) && (
            <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
          )}
          <TouchableOpacity
            style={tailwind("p-3 mx-10 rounded-md my-4 bg-green-600")}
            onPress={submit}
          >
            <Text style={tailwind("text-lg text-center text-white font-bold")}>
              Se connecter
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={tailwind("p-3 mx-10 rounded-md my-4 bg-primary")}
            onPress={() => navigation.navigate('Inscription')}
          >
            <Text style={tailwind("text-lg text-center text-white font-bold")}>
              S'inscrire
            </Text>
          </TouchableOpacity>
        </ScrollView>
      );
    };
    
    export default Login;
    