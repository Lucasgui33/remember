import React, { useState } from "react";
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
  Image,
} from "react-native";
import { tailwind } from "../../../tailwind";
import * as ImagePicker from "expo-image-picker";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { AuthContext } from "../../contexts/AuthContext";

const Register = ({navigation}) => {
  const { register } = React.useContext(AuthContext);

  const [user, setUser] = useState({
    firstname: "",
    lastname: "",
    login: "",
    avatar: "",
    password: "",
    confirmPassword: "",
  });
  const [noMatchPassword, setNoMatchPassword] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setUser({ ...user, avatar: result.uri });
    }
  };

  const showToast = () => {
    ToastAndroid.show("Registered successfully", ToastAndroid.LONG);
  }
  const errorToast = () => {
    ToastAndroid.show("Registered failed", ToastAndroid.LONG);
  }

  const submit = async () => {
    setIsSubmitted(true);
    
    if (
      user.lastname.length &&
      user.firstname.length &&
      user.login.length &&
      user.password.length &&
      user.confirmPassword.length
    ) {
        if(user.password === user.confirmPassword) {
            setNoMatchPassword(false);
            const newUser = await register(user);
            console.log('newUser : ', newUser)
            if(newUser.login === user.login) {
              showToast();
              navigation.navigate('Login');
            } else {
              errorToast();
            }

        } else {
            setNoMatchPassword(true);
        }
    }
  };
  return (
    <ScrollView>
      <Text style={tailwind("text-base text-center m-2")}>Nom</Text>
      <TextInput
        onChangeText={(e) => setUser({ ...user, lastname: e })}
        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
      />
      {(isSubmitted && !user.lastname.length) && (
        <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
      )}
      <Text style={tailwind("text-base text-center m-2")}>Prenom</Text>
      <TextInput
        onChangeText={(e) => setUser({ ...user, firstname: e })}
        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
      />
      {(isSubmitted && !user.firstname.length) && (
        <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
      )}
      <Text style={tailwind("text-base text-center m-2")}>Pseudo (Login)</Text>
      <TextInput
        onChangeText={(e) => setUser({ ...user, login: e })}
        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
      />
      {(isSubmitted && !user.login.length) && (
        <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
      )}
      <Text style={tailwind("text-base text-center m-2")}>Avatar</Text>
      <View style={tailwind("flex flex-row justify-center")}>
        <TouchableOpacity
          onPress={pickImage}
          style={tailwind(
            "mx-10 border border-primary rounded-full h-32 w-32 flex items-center justify-center"
          )}
        >
          {user.avatar ? (
            <Image
              source={{ uri: user.avatar }}
              style={tailwind("rounded-full h-32 w-32")}
            />
          ) : (
            <Text style={tailwind("text-center text-black text-xl")}>
              <MaterialCommunityIcons
                name="pencil-plus"
                size={32}
                color="black"
              />
            </Text>
          )}
        </TouchableOpacity>
      </View>
      <Text style={tailwind("text-base text-center m-2")}>Mot de passe</Text>
      <TextInput
        onChangeText={(e) => setUser({ ...user, password: e })}
        secureTextEntry
        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
      />
       {(isSubmitted && !user.password.length) && (
        <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
      )}
      <Text style={tailwind("text-base text-center m-2")}>Confirmation du mot de passe</Text>
      <TextInput
        onChangeText={(e) => setUser({ ...user, confirmPassword: e })}
        secureTextEntry
        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
      />
        {noMatchPassword && (
            <Text style={tailwind('text-red-500 text-center')}>Les mots de passe ne correspondent pas !</Text>
        )}
        {(isSubmitted && !user.confirmPassword.length) && (
            <Text style={tailwind('text-red-500 text-center')}>Ce champ est requis !</Text>
        )}
      <TouchableOpacity
        style={tailwind("p-3 mx-10 rounded-md my-4 bg-primary")}
        onPress={submit}
      >
        <Text style={tailwind("text-lg text-center text-white font-bold")}>
          S'inscrire
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default Register;
