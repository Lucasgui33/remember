import React,{useEffect} from 'react'
import { SafeAreaView,Text,Image,Button } from 'react-native'
import { tailwind } from '../../tailwind'
import LottieView from 'lottie-react-native';



const NoNetwork = () => {

    let animation = React.createRef();

    useEffect(() => {
        animation.current.play()
    }, [])



    return(
        <SafeAreaView style={tailwind("p-2 flex justify-center items-center h-full")}>
            <LottieView
                ref={animation}
                loop={true}
                style={{
                   width: 450,
                   height: 450
                }}
                source={require('../../assets/lottie/noInternet.json')}
           />
        </SafeAreaView>
    )
}

export default NoNetwork