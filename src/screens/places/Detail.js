import React,{useEffect,useState} from 'react'
import { View,Text, ScrollView,Modal,TouchableOpacity,Image,StyleSheet,Pressable,TextInput,ToastAndroid,Switch } from 'react-native'
import { tailwind } from '../../../tailwind';
import { REACT_APP_API_URL } from '@env';
import { AntDesign, Ionicons} from '@expo/vector-icons';
import * as ImagePicker from "expo-image-picker";
import lodash from 'lodash'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { renderStars } from '../../utils/RenderStars';

const Detail = ({ route }) => {


    const { place } = route.params;

    const [detail,setDetail] = useState(place)
    const [photos, setPhotos] = useState([])
    const [photosPerso, setPhotosPerso] = useState([])
    const [avis, setAvis] = useState([])
    const [maxImages,setMaxImages] = useState(3)
    const [maxImagesPerso,setMaxImagesPerso] = useState(3)
    const [maxAvis,setMaxAvis] = useState(2)
    const [modalVisible, setModalVisible] = useState(false);
    const [modalPhotos, setModalPhotos] = useState(false);
    const [urlPhoto, setUrlPhoto] = useState("");
    const [modalAvis, setModalAvis] = useState(false);
    const [moyenne,setMoyenne] = useState(0)
    const [userId,setUserId] = useState()

    const [note,setNote] = useState(0)
    const [commentaire,setCommentaire] = useState("")
    const [alreadyComment,setAlreadyComment] = useState(false)


    const updateVisited = () => {
        fetch(REACT_APP_API_URL+"/userPlaceList",{
            method:'POST',
            headers:{
                "Content-Type":"application/json",
            },
            body:JSON.stringify({
                visited:!detail.visited,
                toVisit:!detail.toVisit,
                userId:userId,
                placeId:detail.placeId
            })
        })
        .then((response)=>{
            return response.json()
            console.log(json)
        })
        .then((json)=>{
            setDetail({ ...detail, visited: !detail.visited,toVisit: !detail.toVisit  })
        }).catch((error)=>{
            console.log(error)
        })
    }


    useEffect(() => {
        const getUserId = async () => {
            let user = await AsyncStorage.getItem("userId")
            await setUserId(user)
            await getPhotos(user)
            await getAvis(user)
        }
        getUserId()
    }, []);

    //Récupère une image de la gallerie
    const pickImage = async () => {
        const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

        if (permissionResult.granted === false) {
          alert("Vous devez autoriser l'accès à vos photos :/ ");
          return;
        }

        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [3, 4],
          quality: 1,
        });
    
        console.log(result);

        let image = {
            uri:result.uri,
            extension: result.uri.slice(result.uri.length-4, result.uri.length)
        }
    
        if (!result.cancelled) {
            savePicture(image)
        }
    };

    //Ouvre la camera
    const openCamera = async () => {
        // Ask the user for the permission to access the camera
        const permissionResult = await ImagePicker.requestCameraPermissionsAsync();
    
        if (permissionResult.granted === false) {
          alert("You've refused to allow this appp to access your camera!");
          return;
        }
    
        const result = await ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [3, 4],
            quality: 1,
        });
    
        // Explore the result
        console.log(result);
    
        let image = {
            uri:result.uri,
            extension: result.uri.slice(result.uri.length-4, result.uri.length)
        }
    
        if (!result.cancelled) {
            savePicture(image)
        }
    }

    //Enregistre la photo
    const savePicture = async (picture) => {
        let formdata = new FormData();
        let date = Date.now()
        let name = date+picture.extension
        formdata.append("placeId",place.placeId)
        formdata.append("imagePath",`/photos/${name}`)
        formdata.append("userId", await AsyncStorage.getItem('userId'))
        formdata.append("photo", {uri: picture.uri, name: name, type: 'image/jpeg'})

        fetch(`${REACT_APP_API_URL}/photo`,{
            method:'POST',
            headers:{
                'Content-Type': 'multipart/form-data',
            },
            body: formdata,
        })
        .then((response)=> {
            return response.json();
        })
        .then((json)=>{
            getPhotos()
            setModalVisible(false)
            return json;
        }).catch((error)=>{
            console.error(error);
        });
    }

    //Permet de choisir une note d'étoile
    const renderStarsChoice = (note,size,margin) => {
        switch (note) {
            case 1:
                return(
                     <>
                         <TouchableOpacity onPress={()=>setNote(1)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(2)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(3)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(4)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(5)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                     </>
                )
                break;
 
             case 2:
                 return(
                     <>
                         <TouchableOpacity onPress={()=>setNote(1)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(2)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(3)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(4)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(5)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                     </>
                 )
                 break;
 
             case 3:
                 return(
                     <>
                         <TouchableOpacity onPress={()=>setNote(1)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(2)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(3)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(4)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(5)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                     </>
                 )
                 break;
             case 4:
                 return(
                     <>
                         <TouchableOpacity onPress={()=>setNote(1)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(2)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(3)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(4)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(5)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                         </TouchableOpacity>
                     </>
                 )
                break;
             case 5:
                 return(
                     <>
                         <TouchableOpacity onPress={()=>setNote(1)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(2)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(3)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(4)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>setNote(5)} >
                             <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                         </TouchableOpacity>
                      </>
                 )
                break;
        
            default:
             return(
                 <>
                     <TouchableOpacity onPress={()=>setNote(1)} >
                         <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>setNote(2)} >
                         <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>setNote(3)} >
                         <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>setNote(4)} >
                         <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>setNote(5)} >
                         <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     </TouchableOpacity>
                  </>
             )
                break;
        }
    }

    //Récupère les photos d'un utilisateur
    const getPhotos = (id) => {
        fetch(REACT_APP_API_URL+"/photo/"+place.placeId,{
            method:'GET',
            headers:{
                "Content-Type":"application/json",
            },
        })
        .then((response)=>{
            return response.json()
        })
        .then((json)=>{
            let perso = []
            let other = []

            lodash.forEach(json,(p)=>{
                if(p.userId == (userId ? userId : id)){
                    perso.push(p)
                }else{
                    other.push(p)
                }
            })

            setPhotos(other)
            setPhotosPerso(perso)

            if(maxImages != 3){
                setMaxImages(other.length)
            }
            if(maxImagesPerso != 3){
                setMaxImagesPerso(perso.length)
            }
        }).catch((error)=>{
            console.log(error)
        })
    }

    //Récupère les avis d'un utilisateur
    const getAvis = (id) => {
        fetch(REACT_APP_API_URL+"/avis/"+place.placeId,{
            method:'GET',
            headers:{
                "Content-Type":"application/json",
            },
        })
        .then((response)=>{
            return response.json()
        })
        .then((json)=>{
            let total = 0
            lodash.forEach(json,(a)=>{
                total+= a.rate
            })
            setMoyenne(Math.round(total/json.length))
            setAvis(json)
            if(maxAvis != 2){
                setMaxAvis(json.length)
            }
            checkAlreadyComment(json,id)
        }).catch((error)=>{
            console.log(error)
        })
    }

    //Enregistre l'avis d'un utilisateur
    const saveAvis = () => {
        if(commentaire == ""){
            ToastAndroid.show("Tu dois mettre un commentaire", ToastAndroid.SHORT,ToastAndroid.CENTER);
        }else{
            fetch(REACT_APP_API_URL+"/avis",{
                method:'POST',
                headers:{
                    "Content-Type":"application/json",
                },
                body:JSON.stringify({
                    userId:userId,
                    placeId:place.placeId,
                    rate:note,
                    comment:commentaire
                }),
            })
            .then((response)=> {
                return response.json();
            })
            .then((json)=>{
                console.log("JSON : ",json);
                getAvis(userId)
                setNote(0)
                setCommentaire("")
                setModalAvis(false)
            }).catch((error)=>{
                console.error(error);
            });
        }
       
    }

    //Vérifie si il à déjà mis un avis
    const checkAlreadyComment = (avis,id) => {
        let find = lodash.find(avis,(a)=>{
            return a.userId == id
        })
        if(find){
            setAlreadyComment(true)
        }else{
            setAlreadyComment(false)
        }
    }

    return(
        <ScrollView style={tailwind("px-4")}>
             <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        setModalVisible(!modalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                            <View  style={styles.modalView}>
                                <View style={tailwind("flex flex-row mb-4")}>
                                    <TouchableOpacity
                                        onPress={pickImage}
                                    > 
                                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                                            <Ionicons name="albums" size={24} color="black" />
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={openCamera}
                                    > 
                                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                                            <AntDesign name="camera" size={24} color="black" />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalVisible(!modalVisible)}
                                >
                                    <Text style={styles.textStyle}>Fermer</Text>
                                </Pressable>
                           </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalAvis}
                    onRequestClose={() => {
                        setModalAvis(!modalAvis);
                    }}
                >
                    <View style={styles.centeredView}>
                            <View  style={styles.modalView}>
                                <View style={tailwind("flex flex-row my-4")}>
                                    <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Note :</Text>
                                    {renderStarsChoice(note,24,2)}
                                </View>
                                <View style={tailwind("flex flex-row my-4 items-center ")}>
                                    <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Avis :</Text>
                                    <TextInput
                                        style={tailwind("p-2 flex border border-primary rounded-md my-2")}
                                        onChangeText={(e) => setCommentaire(e)}
                                        multiline
                                        numberOfLines={4}
                                        placeholder={"Laisse un commentaire"}
                                    />
                                </View>
                                <View style={tailwind("flex flex-row")}>
                                    <Pressable
                                        style={[styles.button, styles.buttonValide]}
                                        onPress={() => saveAvis()}
                                    >
                                        <Text style={styles.textStyle}>Valider</Text>
                                    </Pressable>
                                    <Pressable
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={() => setModalAvis(!modalAvis)}
                                    >
                                        <Text style={styles.textStyle}>Annuler</Text>
                                    </Pressable>
                                </View>
                           </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalPhotos}
                    onRequestClose={() => {
                        setmodalPhotos(!modalPhotos);
                    }}
                >
                    <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Image style={tailwind('w-72 h-72 rounded-lg mb-8')} source={{uri:urlPhoto}} />
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalPhotos(!modalPhotos)}
                                >
                                    <Text style={styles.textStyle}>Fermer</Text>
                                </Pressable>
                           </View>
                          
                    </View>
                </Modal>
            </View>
            <View style={tailwind("bg-gray-300 flex flex-col p-4 mt-4 rounded-md")}>
                <View style={tailwind("flex flex-row")}>
                    <Text style={tailwind("mb-2 font-bold text-lg")}>Lieux :</Text>
                    <Text style={tailwind("mb-2 text-lg ml-2")}>{detail.places.placeName} </Text>
                </View>
                <View style={tailwind("flex flex-row")}>
                    <Text style={tailwind("mb-2 font-bold text-lg")}>Ville :</Text>
                    <Text style={tailwind("mb-2 text-lg ml-2")}>{detail.places.city} </Text>
                </View>
                <View style={tailwind("flex flex-row")}>
                    <Text style={tailwind("mb-2 font-bold text-lg")}>Pays :</Text>
                    <Text style={tailwind("mb-2 text-lg ml-2")}>{detail.places.country} </Text>
                </View>
            </View>

            <View style={tailwind("flex flex-row justify-center items-center")}>
                <View style={tailwind("flex flex-row items-center")}>
                    <Text>A visité</Text>
                    <Switch
                        trackColor={{ false: "#81b0ff", true: "#81b0ff" }}
                        thumbColor={"#2E86C1"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={() => updateVisited()}
                        value={!detail.toVisit}
                    />
                    <Text>Visité</Text>
                </View>
            </View>

            <View style={tailwind("flex flex-row my-4")}>
                <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Note :</Text>
                {renderStars(moyenne,24,2)}
            </View>

            <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Mes photos :</Text>

            <View style={tailwind("bg-gray-300 flex flex-col p-4 mb-4 rounded-md")}>
                <View style={tailwind('flex flex-row flex-wrap justify-center ')}>
                    {photosPerso.map((photo,i)=>{
                        if(i<=maxImagesPerso && photo.userId == userId){
                            return(
                                <View key={i} style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                                    <TouchableOpacity onPress={()=>{setUrlPhoto(`${REACT_APP_API_URL}${photo.imagePath}`);setModalPhotos(true)}} >
                                        <Image style={tailwind('w-24 h-24 rounded-lg')} source={{uri:`${REACT_APP_API_URL}${photo.imagePath}`}} />
                                    </TouchableOpacity>
                            </View>
                            )
                        }
                    })}
                    {photosPerso.length <= 4 ?
                        null
                    : photosPerso.length > maxImagesPerso ?  
                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                            <TouchableOpacity onPress={()=>setMaxImagesPerso(photosPerso.length)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind('font-bold')}>+{photosPerso.length -4}</Text>
                            </TouchableOpacity>
                        </View>
                    :
                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                            <TouchableOpacity onPress={()=>setMaxImagesPerso(3)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind('font-bold')}>Réduire</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <TouchableOpacity
                        onPress={() => setModalVisible(true)}
                    > 
                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                            <Ionicons name="add" size={24} color="black" />
                        </View>
                    </TouchableOpacity>
                
                </View>
            </View>
            {/* 
            <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Photos des utilisateurs :</Text>

            <View style={tailwind("bg-gray-300 flex flex-col p-4 mb-4 rounded-md")}>
                <View style={tailwind('flex flex-row flex-wrap justify-center ')}>
                    {photos.map((photo,i)=>{
                        if(i<=maxImages && photo.userId != userId){
                            return(
                                <View key={i} style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                                    <TouchableOpacity onPress={()=>{setUrlPhoto(`${REACT_APP_API_URL}${photo.imagePath}`);setModalPhotos(true)}} >
                                        <Image style={tailwind('w-24 h-24 rounded-lg')} source={{uri:`${REACT_APP_API_URL}${photo.imagePath}`}} />
                                    </TouchableOpacity>
                              </View>
                            )
                        }
                    })}
                    {photos.length <= 4 ?
                        null
                    : photos.length > maxImages ?  
                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                            <TouchableOpacity onPress={()=>setMaxImages(photos.length)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind('font-bold')}>+{photos.length -4}</Text>
                            </TouchableOpacity>
                        </View>
                    :
                        <View style={tailwind("bg-gray-400 rounded-lg h-24 w-24 m-2 justify-center items-center ")}>
                            <TouchableOpacity onPress={()=>setMaxImages(3)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind('font-bold')}>Réduire</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </View>
            */}

            <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Mon avis :</Text>

            <View style={tailwind("bg-gray-300 flex flex-col p-4 mb-4 rounded-md")}>
                {!alreadyComment ? 
                    <View style={tailwind("bg-gray-400 my-2 h-12 rounded-md flex flex-row justify-center")}>
                        <TouchableOpacity onPress={()=>setModalAvis(true)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                            <Text style={tailwind("mr-2 font-bold flex flex-row  justify-center items-center")}>Ajouter un avis</Text>
                        </TouchableOpacity> 
                    </View> 
                :null}
                <View style={tailwind('flex flex-col w-full')}>
                    {avis.map((a,i)=>{
                         if(a.userId == userId ){
                            return(
                                <View key={i} style={tailwind("bg-gray-400 my-2 p-2 rounded-md flex flex-col")}>
                                    <View style={tailwind("flex flex-row")}>
                                        <Text style={tailwind("mr-2 font-bold")}>{a.users[0].firstname}</Text>
                                        {renderStars(a.rate,20,1)}
                                    </View>
                                    <Text style={tailwind("pl-4 pt-2")}>{a.comment}</Text>
                                </View>
                            )
                         }
                    })}

                </View>
            </View>
            {/*
            <Text style={tailwind("mb-2 mr-2 font-bold text-lg")}>Avis :</Text>
            
            <View style={tailwind("bg-gray-300 flex flex-col p-4 mb-4 rounded-md")}>
                <View style={tailwind('flex flex-col w-full')}>
                   
                    {avis.map((a,i)=>{
                         if(i <= maxAvis && a.userId != userId){
                            return(
                                <View key={i} style={tailwind("bg-gray-400 my-2 p-2 rounded-md flex flex-col")}>
                                    <View style={tailwind("flex flex-row")}>
                                        <Text style={tailwind("mr-2 font-bold")}>{a.users[0].firstname}</Text>
                                        {renderStars(a.rate,20,1)}
                                    </View>
                                    <Text style={tailwind("pl-4 pt-2")}>{a.comment}</Text>
                                </View>
                            )
                         }
                    })}

                    {avis.length <= 3  ?
                       null 
                    : avis.length > maxAvis ? 
                        <View style={tailwind("bg-gray-400 my-2 h-12 rounded-md flex flex-row justify-center")}>
                            <TouchableOpacity onPress={()=>setMaxAvis(avis.length)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind("mr-2 font-bold flex flex-row  justify-center items-center")}>Afficher plus  </Text>
                            </TouchableOpacity>
                        </View> 
                    :
                        <View style={tailwind("bg-gray-400 my-2 h-12 rounded-md flex flex-row justify-center")}>
                            <TouchableOpacity onPress={()=>setMaxAvis(2)} style={tailwind("font-bold text-xl w-full h-full justify-center items-center")} >
                                <Text style={tailwind("mr-2 font-bold flex flex-row  justify-center items-center")}>Afficher moins</Text>
                            </TouchableOpacity>
                         </View> 
                    
                    }
                       
                </View>
            </View>
            */}

        </ScrollView>
    )
}


const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
      display:"flex",
      flexDirection:"column"
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
      marginHorizontal:20
    },
    buttonValide: {
        backgroundColor: "#2196F3",
        marginHorizontal:20
      },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });


export default Detail