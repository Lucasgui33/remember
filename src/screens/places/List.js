import React, { useState, useEffect,useContext } from 'react';
import { TextInput, Text, View, Switch, Image, ScrollView, TouchableOpacity } from 'react-native';
import { tailwind } from '../../../tailwind';
import { REACT_APP_API_URL } from '@env';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { PlaceContext } from '../../contexts/PlaceContext';
import { renderStars } from '../../utils/RenderStars';

const List = ({ navigation }) => {

    const { getAllPlaces } = useContext(PlaceContext);
    //Liste des lieux
    const [places, setPlaces] = useState([])
    //recherche
    const [searchText, setSearchText] = useState("");

    //switch
    const [isNotVisited, setIsNotVisited] = useState(true);
    const [isVisited, setIsVisited] = useState(true);
    const toggleSwitch1 = () => setIsVisited(previousState => !previousState);
    const toggleSwitch2 = () => setIsNotVisited(previousState => !previousState);

    useEffect(async () => {
        let id = await AsyncStorage.getItem("userId")
        let places = await getAllPlaces(id)
        setPlaces(places)
    }, []);

    //Listener pour relaod au retour sur la page
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            let id = await AsyncStorage.getItem("userId")
            let places = await getAllPlaces(id)
            setPlaces(places)
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    return (
        <ScrollView style={tailwind("px-4")}>
            <View style={tailwind("p-2")}>
                <TextInput
                    onChangeText={(e) => setSearchText(e)}
                    style={tailwind("border rounded-md p-1")}
                    placeholder={"Rechercher"}
                />
                <View style={tailwind("flex flex-row justify-around")}>
                    <View style={tailwind("flex flex-row items-center")}>
                        <Text>Visité</Text>
                            <Switch
                                trackColor={{ false: "#81b0ff", true: "#81b0ff" }}
                                thumbColor={"#2E86C1"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={toggleSwitch1}
                                value={isVisited}
                            />
                    </View>

                    <View style={tailwind("flex flex-row items-center")}>
                        <Text>Non Visité</Text>
                            <Switch
                                trackColor={{ false: "#81b0ff", true: "#81b0ff" }}
                                thumbColor={"#2E86C1"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={toggleSwitch2}
                                value={isNotVisited}
                            />
                    </View>
                </View>

                <Text style={tailwind("font-bold")} >Liste des lieux enregistrés :</Text>

                {places.map((p, i) => {
                    if (!(p.places.placeName.toLowerCase().search(searchText.toLowerCase()) === -1)) {

                        if (isNotVisited !== p.toVisit && isVisited !== p.visited) {
                            return null;
                        }
                        else if (isNotVisited === false && isVisited === false) {
                            return null;
                        }

                        return (
                            <>
                            <TouchableOpacity key={i} onPress={() => navigation.navigate("Details", { place: p })} >
                                <View style={p.visited ?
                                    tailwind("bg-visited mt-3 pt-2 px-4 pb-0 rounded-lg overflow-hidden mb-2") :
                                    tailwind("bg-tovisited mt-3 pt-2 px-4 pb-0 rounded-lg overflow-hidden mb-2")
                                }>
                                    <View style={tailwind(" rounded-xl flex p-4")}>
                                        <View style={tailwind("flex flex-row items-center")}>
                                            <View style={tailwind("bg-gray-400 rounded-md flex items-center justify-center w-20 h-20")}>
                                                {p.photos && p.photos[0] ?
                                                    <Image style={tailwind('w-20 h-20 rounded-md')} source={{ uri: `${REACT_APP_API_URL}${p.photos[0].imagePath}` }} />
                                                    : null}
                                            </View>
                                            <View style={tailwind("flex flex-col ")}>
                                                <View style={tailwind("flex flex-row ")}>
                                                    <Text style={tailwind("ml-4 text-black ")}>{p.places.placeName}, </Text>
                                                    <Text>{p.places.city}</Text>
                                                </View>
                                                
                                                <Text style={tailwind("ml-4 ")}>{p.places.country}</Text>
                                                <View style={tailwind("flex flex-row ml-4 mt-2")}>{renderStars(p.rate,20,0)}</View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                          </>
                        )
                    }
                })}
            </View>
        </ScrollView>
    )
}
export default List