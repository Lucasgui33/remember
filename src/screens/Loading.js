import React from 'react'
import { View,Text,StyleSheet,StatusBar,ActivityIndicator } from 'react-native'
import { tailwind } from '../../tailwind'

const Loading = ({navigation}) => {

    return(
        <View style={tailwind("h-full justify-center items-center pt-8")}>
           <ActivityIndicator size={64} color={"blue"} />
        </View>
    )
}

export default Loading

