import React, { useContext, useEffect, useState } from "react";
import { ScrollView, View, Text, Image, TextInput, TouchableOpacity } from "react-native";
import { tailwind } from "../../../tailwind";
import * as ImagePicker from "expo-image-picker";
import {  MaterialCommunityIcons } from "@expo/vector-icons";
import { REACT_APP_API_URL } from '@env';
import AsyncStorage from "@react-native-async-storage/async-storage";

import { AuthContext } from "../../contexts/AuthContext";
import { UserContext } from "../../contexts/UserContext";

const Profil = ({navigation}) => {
    const { logout } = useContext(AuthContext);
    const { getUser, updateUser } = useContext(UserContext);
    
    const [user, setUser] = useState({});
    const [profileUpdate, setProfileUpdate] = useState(false);

    const getAndSetUser = async () => {
        const currentId = await AsyncStorage.getItem('userId');
        const currentUser = await getUser(currentId);
        await setUser({
            firstname: currentUser['firstname'],
            lastname: currentUser['lastname'],
            login: currentUser['login'],
            password: currentUser['password'],
            avatar: currentUser['avatarPath'],
        });
    }

    useEffect(() => {
        getAndSetUser();
    }, [profileUpdate]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setProfileUpdate(false);
            getAndSetUser();
        });
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setUser({ ...user, avatar: result.uri });
        }
    };

    const modifyProfile = () => {
        setProfileUpdate(!profileUpdate);
    }

    const validate = async () => {
        const currentId = await AsyncStorage.getItem('userId');
        await updateUser(currentId,user);
        setProfileUpdate(false);
    }

    return (
        <ScrollView>
            <View style={tailwind("flex flex-row justify-center")}>
                { profileUpdate ? 
                    (<TouchableOpacity
                        onPress={pickImage}
                        style={tailwind(
                            "mx-10 mt-2 border border-primary rounded-full h-48 w-48 flex items-center justify-center"
                        )}
                    > 
                        <Image
                            source={{
                                uri: user.avatar ? 
                                    user.avatar.includes('/avatars') ? 
                                    `${REACT_APP_API_URL}${user.avatar}`
                                    : user.avatar
                                : "../../../assets/images/avatar.png",
                            }}
                            style={tailwind("rounded-full h-48 w-48")}
                        />
                    </TouchableOpacity>)
                :
                    (
                    <Image
                        source={{
                            uri: user.avatar ? `${REACT_APP_API_URL}${user.avatar}` : "../../../assets/images/avatar.png"
                        }}
                        style={tailwind("rounded-full mt-2 h-48 w-48")}
                    />
                    )
                }

            </View>
            <View>
            { profileUpdate ? 
                (<>
                    <Text style={tailwind("text-base text-black text-center m-2")}>Nom</Text>
                    <TextInput
                        value={user.lastname}
                        onChangeText={(e) => setUser({ ...user, lastname: e })}
                        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")} 
                    />
                    <Text style={tailwind("text-base text-center m-2")}>Prénom</Text>
                    <TextInput
                        value={user.firstname}
                        onChangeText={(e) => setUser({ ...user, firstname: e })}
                        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                    />
                    <Text style={tailwind("text-base text-center m-2")}>Pseudo (Login)</Text>
                    <TextInput
                        value={user.login}
                        onChangeText={(e) => setUser({ ...user, login: e })}
                        style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                    />
                </>) 
            : 
                (<>
                    <Text style={tailwind("text-center text-black text-xl m-2")}>
                        {user.firstname} {user.lastname}
                    </Text><Text style={tailwind("text-center text-black font-bold text-xl m-2")}>
                        {user.login}
                    </Text>
                </>) 
            }
            </View>
            
            <View>
                <TouchableOpacity
                    style={profileUpdate ? 
                        tailwind("p-3 mx-10 rounded-md my-4 bg-red-600") : 
                        tailwind("p-3 mx-10 rounded-md my-4 bg-primary")
                    }
                    onPress={modifyProfile}
                >
                    <Text style={tailwind("text-lg text-center text-white font-bold")}>
                    { profileUpdate ? 
                        (<Text>Annuler</Text>)
                    :
                        ( 
                        <>
                            <MaterialCommunityIcons
                                name="pencil-plus"
                                size={24}
                                color="white" 
                            />
                            <Text>&nbsp; Modifier le profil</Text>
                        </>
                        )
                    }
                    </Text>
                </TouchableOpacity>
            </View>
            { profileUpdate ? (
                <View>
                    <TouchableOpacity
                        style={tailwind("p-3 mx-10 rounded-md my-4 bg-green-600")}
                        onPress={validate}
                    >
                        <Text style={tailwind("text-lg text-center text-white font-bold")}>Valider</Text>
                    </TouchableOpacity>
                </View>
            ) : (
                <View>
                    <TouchableOpacity
                        style={tailwind("p-3 mx-10 rounded-md my-4 bg-red-600")}
                        onPress={() => logout({navigation})}
                    >
                        <Text style={tailwind("text-lg text-center text-white font-bold")}>Déconnexion</Text>
                    </TouchableOpacity>
                </View>
            )}
        </ScrollView>
    );
};

export default Profil;
