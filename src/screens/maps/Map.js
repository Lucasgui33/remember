import React, { useState, useEffect,useRef, useContext } from 'react';
import MapView, { Marker} from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions,SafeAreaView,TouchableOpacity, TextInput,ToastAndroid,Image,Switch } from 'react-native';
import * as Location from 'expo-location';
import { tailwind } from '../../../tailwind';
import { Ionicons,MaterialIcons,MaterialCommunityIcons} from '@expo/vector-icons';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Loading from '../Loading';
import { REACT_APP_API_URL } from '@env';
import { darkMapStyle,lightMapStyle } from '../../datas/datas';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { PlaceContext } from "../../contexts/PlaceContext";
import { renderStars } from '../../utils/RenderStars';

const Map = ({navigation}) => {

    const ref = useRef();
    const mapRef = useRef()

    //Context Places
    const { getAllPlaces,addPlaces } = useContext(PlaceContext);

    //Hauteur de base pour la vue sur la Map
    const delta = {
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    }

    //ID de l'utilisateur en cours
    const [userId,setUserId] = useState(null)
    //Localisation à l'arrivée sur l'app
    const [location, setLocation] = useState(null)
    //Localisation de la recherche
    const [searchLocation, setSearchLocation] = useState(null)
    //Localisation actuel de la vue sur la maps
    const [actualPosition, setActualPosition] = useState(null)
    //Informations sur le marker cliquer
    const [infosLocation, setInfosLocation] = useState("")
    //Mode sombre/clair
    const [darkMode, setDarkmode] = useState(false)
    //Localisation de l'ajout d'un point
    const [newPlaces, setNewPlaces] = useState()
    //Affiche une modfal pour l'ajout d'un lieux
    const [showModal,setShowModal] = useState(false)
    //Affiche une modal pour l'ajout d'un lieux suite à une recherche
    const [showModalSearch,setShowModalSearch] = useState(false)
    //Liste des markers
    const [markers, setMarkers] = useState([])

    //Vérifie si on a remplie toutes les infos d'un lieux avant de l'ajouter
    const checkIsComplete = () => {
        if(newPlaces.placeName == "" || newPlaces.city == "" || newPlaces.country == "" || newPlaces.latitude == "" || newPlaces.longitude == "" ){
            return true
        }else{
            return false
        }
    }

    //Supprime le champs de recherche
    const clearTextGooglePlaces = () => {
        ref.current?.setAddressText('');
        setSearchLocation(null)
        setInfosLocation("")
    }

    //Raccourci le nom dans l'input recherche
    const cutName = (text) => {
        let newtext = text.substring(0,30)
        newtext = newtext + " ... "
        return newtext
    }

    //Ajoute un lieux
    const ajoutLieux = async () => {
        let add = await addPlaces(newPlaces)
        if(add){
            //Reinitialisation
            setNewPlaces({
                placeName:"",
                city:"",
                country:"",
                latitude:"",
                longitude:"",
                show:false,
                userId:userId,
                visited:false,
                toVisit:true
            })
            setShowModal(false)
            setShowModalSearch(false)
            setSearchLocation(null)
            setInfosLocation(null)
            setTimeout(() => {
                ToastAndroid.show("Add place successfully", ToastAndroid.LONG,ToastAndroid.BOTTOM);
            }, 300);
            let places = await getAllPlaces(userId)
            setInfosLocation("")
            setMarkers(places)
        }else{
            ToastAndroid.show("Add place failed. An error occured..", ToastAndroid.LONG,ToastAndroid.BOTTOM);
        }
    }

    useEffect(() => {
        (async () => {
            let user = await AsyncStorage.getItem('userId')
            //Ajoute un listener pour recharger les lieux au retour sur la page
            navigation.addListener('focus', async () => {
                setMarkers([])
                let p = await  getAllPlaces(user);
                setInfosLocation("")
                setMarkers(p)
            });
            let places = await getAllPlaces(user)
            setInfosLocation("")
            setMarkers(places)
            setNewPlaces({
                placeName:"",
                city:"",
                country:"",
                latitude:"",
                longitude:"",
                show:false,
                userId:user,
                visited:false,
                toVisit:true
            })
            setUserId(user)
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                alert("Pas de gps")
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            let coords = {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: delta.latitudeDelta,
                longitudeDelta:delta.longitudeDelta,
                nom: "Tu es ici"
            }
            
            setLocation(coords)
            setActualPosition(coords)
        })();
    }, []);

    //Récupère les infos d'un lieux, nom,ville,pays
    const getInformations = (tab) => {
        let place = {
            placeName:"",
            city:"",
            country:""
        }

        tab.map((item)=>{
            if(item.types.includes("route")){
                place.placeName = item.long_name
            }else if (item.types.includes("country")){
                place.country = item.long_name
            }else if (item.types.includes("locality")){
                place.city = item.long_name
            }
        })
        return place
    }

    if(location == null){
        return <Loading/>
    }

    return (
        <SafeAreaView  style={styles.container}>

            <GooglePlacesAutocomplete
                ref={ref}
                placeholder='Rechercher'
                fetchDetails={true}
                onPress={(data, details = null) => {
                    let infosLocation = getInformations(details.address_components)
                    setSearchLocation({
                        latitude:details.geometry.location.lat,
                        longitude:details.geometry.location.lng,
                        latitudeDelta: delta.latitudeDelta,
                        longitudeDelta:delta.longitudeDelta,
                        nom:data.description,
                        placeName:infosLocation.placeName,
                        city:infosLocation.city,
                        country:infosLocation.country
                    })
                    let split = data.description.split(',')
                    setNewPlaces({
                        placeName:split[0],
                        city:infosLocation.city,
                        country:infosLocation.country,
                        latitude:details.geometry.location.lat,
                        longitude:details.geometry.location.lng,
                        show:false,
                        userId:userId,
                        visited:false,
                        toVisit:true
                    })
                    mapRef.current?.animateToRegion({
                        latitude:details.geometry.location.lat,
                        longitude:details.geometry.location.lng,
                        latitudeDelta: delta.latitudeDelta,
                        longitudeDelta:delta.longitudeDelta,
                    })
                    ref.current?.setAddressText(cutName(data.description));
                }}
                onFail={(error) => console.log(error)}
                query={{
                    key: 'AIzaSyDFjsyiwjhVP3aM_dGPF6wDck7Xfjh61h8',
                    language: 'en',
                    type:["address","establishment"]
                }}
                renderRightButton={() => (
                    <TouchableOpacity
                       style={tailwind("absolute right-0 mr-4 mt-3")}
                        onPress={clearTextGooglePlaces}
                    >
                        <Ionicons
                            name="close-circle-outline"
                            size={20}
                            color={darkMode ? "black" : "white"}
                        />
                    </TouchableOpacity>
                )}
               
                styles={
                    {
                        container:{
                            flex:0,
                            position:"absolute",
                            width:"90%",
                            zIndex:1,
                            marginTop:40,
                            marginHorizontal:20,
                        },
                        listView:{
                            backgroundColor: darkMode? 'white' : "black",
                            opacity:0.8,
                        },
                        textInput:{
                            height:50,
                            backgroundColor:darkMode? 'white' : "black",
                            opacity:0.8,
                            color:darkMode? 'black' : "white",
                            
                        },
                        row:{
                            backgroundColor: darkMode ? 'white' : "black",
                        },
                        description:{
                            color:darkMode? 'black' : "white"
                        },
                        powered:{
                            backgroundColor: darkMode ? 'white' : "black",
                        },
                        poweredContainer:{
                            backgroundColor: darkMode ? 'white' : "black",
                        }
                    }}
                />

            <MapView style={styles.map}
                ref={mapRef}
                initialRegion={location}
                region={actualPosition}
                onPress={()=>setInfosLocation("")}
                onRegionChangeComplete={(coords)=>setActualPosition(coords)}
                customMapStyle={darkMode ? darkMapStyle : lightMapStyle}

            >
                {searchLocation && 
                    <Marker
                        pinColor="#BF40B3"
                        coordinate={searchLocation}
                        title={searchLocation.nom}
                        onPress={()=>setShowModalSearch(true)}
                    />
                }

                {location && 
                    <Marker
                        coordinate={location}
                        title={location.nom}
                        onPress={()=>setInfosLocation("")}
                    />
                }

                {newPlaces.show && 
                    <Marker
                        draggable={true}
                        onDragEnd={(e) => {
                            {
                                setNewPlaces({
                                    ...newPlaces,
                                    latitude:e.nativeEvent.coordinate.latitude,
                                    longitude:e.nativeEvent.coordinate.longitude,
                                })};
                                setShowModal(true);
                                mapRef.current?.animateToRegion({
                                    latitude:e.nativeEvent.coordinate.latitude,
                                    longitude:e.nativeEvent.coordinate.longitude,
                                    latitudeDelta: delta.latitudeDelta,
                                    longitudeDelta:delta.longitudeDelta,
                                })
                            }
                        }
                        coordinate={newPlaces}
                        title={"Déplace moi !"}
                        description={`${newPlaces.latitude.toString()}, ${newPlaces.longitude.toString()}`}
                        pinColor="#8a1278"
                    />
                }

                {markers.map((mark, i) => {
                    if(mark.userId == userId){
                        return (
                            <Marker
                                key={i}
                                coordinate={mark.places}
                                title={mark.places.placeName}
                                pinColor={mark.visited ? "#274D74" : mark.toVisit ? "#53C6C4" : "#22b32c"}
                                onPress={()=>setInfosLocation(mark)}
                            />
                        )
                    }
                   
                })}

            </MapView>
            
            <View style={tailwind("absolute flex flex-row justify-between w-full")}>
                <TouchableOpacity
                    style={darkMode ?
                        tailwind("flex flex-row mt-28 bg-white opacity-80 rounded-full h-10 w-10 justify-center items-center ml-4")
                        :
                        tailwind("flex flex-row mt-28 bg-black opacity-80 rounded-full h-10 w-10 justify-center items-center ml-4")
                    }
                    onPress={
                        ()=>{
                            mapRef.current?.animateToRegion({
                                latitude:location.latitude,
                                longitude:location.longitude,
                                latitudeDelta: delta.latitudeDelta,
                                longitudeDelta:delta.longitudeDelta,
                            })
                        }
                    }
                >
                    <Ionicons 
                        name="md-locate-outline"
                        size={30}
                        color={darkMode ? "black" : "white"}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    style={darkMode ?
                        tailwind("flex flex-row justify-end mt-28 bg-white opacity-80 rounded-full h-10 w-10 justify-center items-center mr-4")
                        :
                        tailwind("flex flex-row justify-end mt-28 bg-black opacity-80 rounded-full h-10 w-10 justify-center items-center mr-4")
                    }
                    onPress={()=>setNewPlaces({...newPlaces,latitude:actualPosition.latitude,longitude:actualPosition.longitude,show:true})}
                >
                    <MaterialIcons 
                        name="location-pin"
                        size={26}
                        color={darkMode ? "black" : "white"}
                    />
                   
                </TouchableOpacity>
            </View>
            <View style={tailwind("absolute flex flex-row justify-between w-full")}>
                <TouchableOpacity
                    style={darkMode ?
                        tailwind("flex flex-row mt-40 bg-white opacity-80 rounded-full h-10 w-10 justify-center items-center ml-4")
                        :
                        tailwind("flex flex-row mt-40 bg-black opacity-80 rounded-full h-10 w-10 justify-center items-center ml-4")
                    }
                    onPress={()=>setDarkmode(!darkMode)}
                >
                    <MaterialCommunityIcons
                        name="theme-light-dark"
                        color={darkMode ? "black" : "white"}
                        size={24}
                    />
                </TouchableOpacity>
            </View>
           { infosLocation ? 
                <View style={tailwind("absolute bottom-0 flex w-full flex-row mb-8 h-36")}>
                    <View style={tailwind("bg-white border border-gray-300 rounded-3xl flex w-10/12 mx-8 p-4")}>
                       <View style={tailwind("flex flex-row h-full")}>
                            <View style={tailwind("bg-red-400 rounded-md h-full w-1/3")}>
                                {infosLocation.photos && infosLocation.photos[0] ? 
                                    <Image style={tailwind('w-full h-full rounded-lg')} source={{uri:`${REACT_APP_API_URL}${infosLocation.photos[0].imagePath}`}} />
                                :null}
                            </View>
                            <TouchableOpacity onPress={()=>navigation.navigate("MapsDetails",{place:infosLocation})} style={tailwind("flex flex-col h-full w-2/3")}>
                                <Text style={tailwind("ml-4 overflow-hidden w-48")}>{`${infosLocation.places.placeName}, ${infosLocation.places.city}`}</Text>
                                <Text style={tailwind("ml-4 mt-4")}>{infosLocation.places.country}</Text>
                                <View style={tailwind("flex flex-row ml-4 mt-4")}>{renderStars(infosLocation.rate,20,0)}</View>
                            </TouchableOpacity>
                       </View>
                    </View>
                </View>
            :null}

            { showModal ? 
                <View style={tailwind("absolute bottom-0 flex w-full flex-row h-80")}>
                    <View style={tailwind("bg-white border border-gray-300 rounded-t-3xl flex flex-col w-full p-4")}>
                        <Text style={tailwind("text-center underline font-bold")}>Ajout d'un nouveau point</Text>
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, placeName: e })}
                            placeholder={"Lieux"}
                        />
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, city: e })}
                            placeholder={"Ville"}
                        />
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, country: e })}
                            placeholder={"Pays"}
                        />
                        <View style={tailwind("flex flex-row justify-around")}>
                            <View style={tailwind("flex flex-row items-center")}>
                                <Text>A visité</Text>
                                <Switch
                                    trackColor={{ false: "#81b0ff", true: "#81b0ff" }}
                                    thumbColor={"#2E86C1"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={() => setNewPlaces({ ...newPlaces, visited: !newPlaces.visited,toVisit: !newPlaces.toVisit  })}
                                    value={!newPlaces.toVisit}
                                />
                                <Text>Visité</Text>
                            </View>
                        </View>
                        
                        <View style={tailwind("flex flex-row justify-around mt-2")}>
                            <TouchableOpacity 
                                disabled={checkIsComplete()}
                                onPress={()=>ajoutLieux()}
                                style={checkIsComplete() ? tailwind("bg-gray-300 py-2 px-4 rounded-md") : tailwind("bg-green-300 py-2 px-4 rounded-md")}>
                                <Text>Valider</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                onPress={() =>
                                    {
                                        setNewPlaces({
                                            placeName:"",
                                            city:"",
                                            country:"",
                                            latitude:"",
                                            longitude:"",
                                            show:false,
                                            visited:false,
                                            toVisit:true,
                                            userId:userId
                                        });
                                        setShowModal(false)
                                    }
                                } 
                            style={tailwind("bg-red-300 py-2 px-4 rounded-md")}>
                                <Text>Annuler</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            :null}

            { showModalSearch ? 
                <View style={tailwind("absolute bottom-0 flex w-full flex-row h-80")}>
                    <View style={tailwind("bg-white border border-gray-300 rounded-t-3xl flex flex-col w-full p-4")}>
                        <Text style={tailwind("text-center underline font-bold")}>Ajout d'un nouveau point</Text>
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, placeName: e })}
                            placeholder={"Lieux"}
                            defaultValue={newPlaces.placeName}
                        />
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, city: e })}
                            placeholder={"Ville"}
                            defaultValue={newPlaces.city}
                        />
                        <TextInput
                            style={tailwind("p-2 mx-10 border border-primary rounded-md my-2")}
                            onChangeText={(e) => setNewPlaces({ ...newPlaces, country: e })}
                            placeholder={"Pays"}
                            defaultValue={newPlaces.country}
                        />

                        <View style={tailwind("flex flex-row justify-center items-center -mb-3 -mt-2")}>
                            <View style={tailwind("flex flex-row items-center")}>
                                <Text>A visité</Text>
                                <Switch
                                    trackColor={{ false: "#81b0ff", true: "#81b0ff" }}
                                    thumbColor={"#2E86C1"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={() => setNewPlaces({ ...newPlaces, visited: !newPlaces.visited,toVisit: !newPlaces.toVisit  })}
                                    value={!newPlaces.toVisit}
                                />
                                <Text>Visité</Text>
                            </View>
                        </View>
                        
                        <View style={tailwind("flex flex-row justify-around mt-2")}>
                            <TouchableOpacity 
                                disabled={checkIsComplete()}
                                onPress={()=>{clearTextGooglePlaces();ajoutLieux()}}
                                style={checkIsComplete() ? tailwind("bg-gray-300 py-2 px-4 rounded-md") : tailwind("bg-green-300 py-2 px-4 rounded-md")}>
                                <Text>Valider</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                onPress={() =>
                                    {
                                        setNewPlaces({
                                            placeName:"",
                                            city:"",
                                            country:"",
                                            latitude:"",
                                            longitude:"",
                                            show:false,
                                            userId:userId,
                                            visited:false,
                                            toVisit:true
                                        });
                                        setShowModalSearch(false);
                                    }
                                } 
                            style={tailwind("bg-red-300 py-2 px-4 rounded-md")}>
                                <Text>Annuler</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            :null}
        </SafeAreaView >
        
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',

    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
});


export default Map