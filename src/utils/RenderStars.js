import { AntDesign  } from '@expo/vector-icons';
import { tailwind } from '../../tailwind';
import React from 'react';

export const renderStars = (note,size,margin) => {
    switch (note) {
        case 1:
            return(
                 <>
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 </>
            )
            break;

         case 2:
             return(
                 <>
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 </>
             )
             break;

         case 3:
             return(
                 <>
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 </>
             )
             break;
         case 4:
             return(
                 <>
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 </>
             )
            break;
         case 5:
             return(
                 <>
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                     <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#FFDA00" />
                  </>
             )
            break;
    
        default:
         return(
             <>
                 <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
                 <AntDesign style={tailwind(`mx-${margin}`)} name="star" size={size} color="#989898" />
              </>
         )
            break;
    }
 }