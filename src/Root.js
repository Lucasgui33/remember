import React from "react";
import Main from "./Main";
import { AuthProvider } from "./contexts/AuthContext"
import { UserProvider } from "./contexts/UserContext"
import { PlaceProvider } from "./contexts/PlaceContext";

const Root = () => {
  return (
    <AuthProvider>
      <UserProvider>
        <PlaceProvider>
          <Main />
        </PlaceProvider>
      </UserProvider>
    </AuthProvider>
  );
};

export default Root;