import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Map from "../screens/maps/Map";
import Detail from "../screens/places/Detail";

const Stack = createNativeStackNavigator();

const StackMaps = () => {
    return (
        <Stack.Navigator initialRouteName="Map">
            <Stack.Screen
                options={{ headerShown: false }}
                name="Map"
                component={Map}
            />
            <Stack.Screen
                name="MapsDetails"
                options={{
                headerTitle: "Détails du lieu",
                headerStyle: {
                    backgroundColor: "#2E86C1",
                },
                headerTintColor: 'white',
                }}
                component={Detail}
            />
        </Stack.Navigator>
    );
};

export default StackMaps;
