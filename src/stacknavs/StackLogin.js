import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Login from '../screens/auth/Login';
import Register from '../screens/auth/Register';

const Stack = createNativeStackNavigator();

const StackLogin = () => {
    return (
        <Stack.Navigator initialRouteName="Login" >
            <Stack.Screen 
                name="Login" 
                options={{
                    headerTitle: "Connexion",
                    headerStyle: {
                        backgroundColor: "#2E86C1",
                    },
                    headerTintColor: 'white',
                }}
                component={Login}
            />
            <Stack.Screen 
                name="Inscription" 
                options={{
                    headerStyle: {
                        backgroundColor: "#2E86C1",
                    },
                    headerTintColor: 'white',
                }}
                component={Register}
                />
        </Stack.Navigator>
    )
}

export default StackLogin