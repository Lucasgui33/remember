import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Detail from "../screens/places/Detail";
import List from "../screens/places/List";

const Stack = createNativeStackNavigator();
const StackPlaces = () => {
    return (
        <Stack.Navigator initialRouteName="List">
            <Stack.Screen
                name="List"
                options={{
                    headerTitle: "Liste des lieux",
                    headerStyle: {
                        backgroundColor: "#2E86C1",
                    },
                    headerTintColor: 'white',
                }}
                component={List}
            />
            <Stack.Screen 
                name="Details" 
                options={{
                    headerTitle: "Détail du lieu",
                    headerStyle: {
                        backgroundColor: "#2E86C1",
                    },
                    headerTintColor: 'white',
                }}
                component={Detail} />
        </Stack.Navigator>
    );
};

export default StackPlaces;
