import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Profil from "../screens/profil/Profil";

const Stack = createNativeStackNavigator();

const StackProfil = () => {
    return (
        <Stack.Navigator initialRouteName="User">
            <Stack.Screen
                name="User"
                options={{
                    headerTitle: "Mon Profil",
                    headerStyle: {
                        backgroundColor: "#2E86C1",
                    },
                    headerTintColor: 'white',
                }}
                component={Profil}
            />
        </Stack.Navigator>
    );
};

export default StackProfil;
