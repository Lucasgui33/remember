import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Register from '../screens/auth/Register';

const Stack = createNativeStackNavigator();

const StackRegister = () => {
    return (
        <Stack.Navigator initialRouteName="Register" >
            <Stack.Screen name="Register" component={Register}/>
        </Stack.Navigator>
    )
}

export default StackRegister

