import React, { useState,useEffect, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from "@expo/vector-icons";
import { StatusBar } from 'expo-status-bar';
import NetInfo from '@react-native-community/netinfo';

//Importer Stack Map
import StackMaps from './stacknavs/StackMaps';

//Importer Stack Profile
import StackProfil from './stacknavs/StackProfil';

//Importer Stack Liste des lieux
import StackPlaces from './stacknavs/StackPlaces';

//Importer Stack Connexion/Inscription
import StackLogin from './stacknavs/StackLogin';

const Tab = createBottomTabNavigator();

import NoNetwork from './screens/NoNetwork';
import { AuthContext } from './contexts/AuthContext';

//Checker le GPS

export default function Main () {

    const {isConnected} = useContext(AuthContext)

    //Checker internet
    const [hasInternet, setHasInternet] = useState(true)

    //Checker la connexion
    useEffect(() => {

        NetInfo.addEventListener(networkState => {
            setHasInternet(networkState.isConnected)
        });

       /* NetInfo.fetch().then((res)=>{
            setHasInternet(res.isConnected)
        })

        const unsuscribe = NetInfo.addEventListener((state)=>{
            console.log("state", state)
        })
        return(
            unsuscribe()
        )*/

    }, []);

    if(!hasInternet){
        return(
            <NoNetwork/>
        )
    }

    return (
        <NavigationContainer>
             <StatusBar style="auto" />
            {isConnected ? 
             <Tab.Navigator screenOptions={({ route }) => ({
                headerShown: false,
                tabBarShowLabel:false,
                tabBarActiveBackgroundColor: "#2E86C1",
                tabBarInactiveBackgroundColor: "#2E86C1",
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'stackMaps') {
                        iconName = focused
                            ? 'map'
                            : 'map-outline';
                    } else if (route.name === 'stackPlaces') {
                        iconName = focused ? 'list' : 'list-outline';
                    } else if (route.name === 'stackProfil') {
                        iconName = focused ? 'person-circle' : 'person-circle-outline';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'white',
                tabBarInactiveTintColor: 'white',
            })}>
                <Tab.Screen name="stackMaps" component={StackMaps} />
                <Tab.Screen name="stackPlaces" component={StackPlaces} />
                <Tab.Screen name="stackProfil" component={StackProfil} />
            </Tab.Navigator>
            :
                <StackLogin/>
            }
        </NavigationContainer>
  )
}