module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        "ydays":"#ec46a2",
        "primary":"#2E86C1",
        "visited":"#274D74",
        "tovisited":"#53C6C4"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
