# Remember

## Description de l'appplication

- Carte avec des points d'intérêts représentant les lieux visités et les lieux à visiter
- Affichage sous forme de liste
- Commentaires et likes sur les photos des autres utilisateurs

### Installation

- Clonez le projet
- Installation des packages

```
npm install
```

### Lancement

```
npm start
```
